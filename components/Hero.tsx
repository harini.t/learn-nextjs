"use client";
import Image from 'next/image';
import CustomButton from './CustomButton';


const Hero = () => {
    const handleScroll = () => {
    }

    return (
        <div className='hero'>
            <div className="flex-1 pt-36 padding-x">
                <h1 className="hero__title">
                    Welcome to ENDE 2024
                </h1>
                <p className="hero__subtitle">
                It is our great pleasure to announce the 27th International Workshop on Electromagnetic Nondestructive Evaluation (ENDE) 2024, to be held in India from 24 to 26 May, 2024.
                </p>
                <CustomButton 
                title= "Explore"
                containerStyles="bg-primary-blue 
                text-white rounded-full mt-10"
                handleClick={handleScroll}
                />
            </div>
            <div className="hero__image-container">
                <div className="hero__image">
                    <Image src="/hero.png" alt="hero" 
                    fill className="object-contain"/>
                </div>
                <div className='hero__image-overlay'/>
            </div>
        </div>
    )
}

export default Hero 